# Utilities

This is a PCMRBot.js module that provides commonly used, generic utilities used by multiple other services. 

There is no suggested limit to the number of instances of this module you can run.

## Dependencies

This module has no dependencies.

## Environment variables

The following environment variables should be set prior to launch. 

* Broker options
  * (optional) `TRANSPORT_NODE_ID` - The node ID that should be used for connecting to the rest of the cluster. Defaults to the system hostname plus a random number string. 
  * (optional) `TRANSPORT_BIND_ADDRESS` - The network address that the service broker should bind to. Defaults to all available addresses across all network interfaces.

# API reference

<a name="module_utilities.text"></a>

## utilities.text
This module exposes general purpose utilities to work with text strings.

**Version**: 1  

* [utilities.text](#module_utilities.text)
    * [.markdownToMrkdwn(string)](#module_utilities.text.markdownToMrkdwn) ⇒ <code>string</code>
    * [.truncateString(string, length, [ending])](#module_utilities.text.truncateString) ⇒ <code>string</code>

<a name="module_utilities.text.markdownToMrkdwn"></a>

### utilities.text.markdownToMrkdwn(string) ⇒ <code>string</code>
Converts a string from Markdown to the Slack mrkdwn format.

**Kind**: static method of [<code>utilities.text</code>](#module_utilities.text)  
**Returns**: <code>string</code> - The string, converted to mrkdwn.  

| Param | Type | Description |
| --- | --- | --- |
| string | <code>string</code> | The string to convert |

<a name="module_utilities.text.truncateString"></a>

### utilities.text.truncateString(string, length, [ending]) ⇒ <code>string</code>
Truncates a string if it is longer than a specified length.

**Kind**: static method of [<code>utilities.text</code>](#module_utilities.text)  
**Returns**: <code>string</code> - The string, truncated at the specified length.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| string | <code>string</code> |  | The string to truncate |
| length | <code>number</code> |  | The length of the string that it should be truncated at |
| [ending] | <code>string</code> | <code>&quot;...&quot;</code> | The string that should be used to indicate a truncated string |

