module.exports = {
	apps : [{
		name      : 'core/utilities',
		script    : 'build/utilities.js',
		env: {
			// Either inject the values via environment variables or define them here
			TRANSPORT_BIND_ADDRESS: process.env.TRANSPORT_BIND_ADDRESS || ""
		}
	}]
};
