import { Service, Context } from "moleculer";
import * as htmlToMrkdwn from "html-to-mrkdwn";
import * as snuownd from "snuownd";

/**
 * This module exposes general purpose utilities to work with text strings. 
 * 
 * @module "utilities.text"
 * @version 1
 */
class TextService extends Service {
	constructor(broker) {
		super(broker);

		this.parseServiceSchema({
			name: "utilities.text",
			version: 1,
			actions: {
				truncateString: {
					name: "truncateString",
					params: {
						string: "string",
						length: "number",
						ending: {type: "string", optional: true}
					},
					handler: this.truncateString
				},
				markdownToMrkdwn: {
					name: "markdownToMrkdwn",
					params: {
						string: "string"
					},
					handler: this.markdownToMrkdwn
				}
			}
		});
	}

	/**
	 * Converts a string from Markdown to the Slack mrkdwn format.
	 * @function
	 * @static
	 * @name markdownToMrkdwn
	 * @param {string} string - The string to convert
	 * @returns {string} The string, converted to mrkdwn.
	 */
	async markdownToMrkdwn(ctx: Context) {
		// This is what I call weaponized laziness. 
		let html = snuownd.getParser().render(ctx.params.string);
		let mrkdwn = htmlToMrkdwn(html);

		return mrkdwn.text
	}

	/**
	 * Truncates a string if it is longer than a specified length.
	 * @function 
	 * @static
	 * @name truncateString
	 * @param {string} string - The string to truncate
	 * @param {number} length - The length of the string that it should be truncated at
	 * @param {string} [ending=...] - The string that should be used to indicate a truncated string
	 * @returns {string} The string, truncated at the specified length.
	 */
	async truncateString(ctx: Context) {
		let ending = ctx.params.ending || "...";

		if (ctx.params.string.length > ctx.params.length) {
			return ctx.params.string.substr(0, ctx.params.length - ending.length) + ending;
		} else {
			return ctx.params.string;
		}
	}
}

module.exports = TextService;